<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style type="text/css">
      caption {
            background-color: yellow;
        }
       th
        {
            background-color: gray;
        }
        tr#verde {
            background-color: lightgreen;
        }
        table{
            border: 1px solid red;
            width: 500px;
            border-collapse: collapse;
        }
        th,td{
            border: 1px solid black;
        }
    </style>
</head>
<body>
    <?php
     echo "<table>";
     echo "<tr>";
     echo "<caption>Productos</caption>";
     echo "</tr>";
     echo "<tr>";
       echo "<th>Nombre</th>";
       echo "<th>Cantidad</th>";
       echo "<th>Precio</th>";
     echo "</tr>";
     echo "<tr>";
       echo "<td>Coca Cola</td>";
       echo "<td>100</td>";
       echo "<td>4.500</td>";
     echo "</tr>";
     echo '<tr id="verde">';
     echo "<td>Pepsi</td>";
     echo "<td>30</td>";
     echo "<td>4.800</td>";
     echo "</tr>";

     echo "<tr>";
     echo "<td>Sprite</td>";
     echo "<td>20</td>";
     echo "<td>4.500</td>";
     echo "</tr>";

     echo '<tr id="verde">';
     echo "<td>Guaraná</td>";
     echo "<td>200</td>";
     echo "<td>4.500</td>";
     echo "</tr>";
     
     echo "<tr>";
     echo "<td>SevenUP</td>";
     echo "<td>24</td>";
     echo "<td>4.500</td>";
     echo "</tr>";

     echo '<tr id="verde">';
     echo "<td>Mirinda Naranja</td>";
     echo "<td>56</td>";
     echo "<td>4.800</td>";
     echo "</tr>";

     echo "<tr>";
     echo "<td>Mirinda Guaraná</td>";
     echo "<td>89</td>";
     echo "<td>4.800</td>";
     echo "</tr>";

     echo '<tr id="verde">';
     echo "<td>Fanta Naranja</td>";
     echo "<td>10</td>";
     echo "<td>4.500</td>";
     echo "</tr>";

     echo "<tr>";
     echo "<td>Fanta Piña</td>";
     echo "<td>2</td>";
     echo "<td>4.500</td>";
     echo "</tr>";

   echo "</table>"; 

?>
</body>
</html>