
create table ALUMNOS (
    ID          SERIAL               not null,
   NOMBRE       VARCHAR(100)         not null,
   APELLIDO     VARCHAR(100)         not null,
   EDAD               INT            not null,
   constraint PK_ALUMNO primary key (ID)
);

