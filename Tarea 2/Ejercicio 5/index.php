<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Ejercicio5</title>
<link href="css/estilo_tabla.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<?php

$tablaDel9 = "";
$variable  = 9;

$tablaDel9.=<<<HTML
	<table>
		<tr>
			<th>Regla</th>
			<th>Resultado</th>
		</tr>
HTML;


for ($i=1; $i <= 10; $i++) { 

	$regla     = "$variable * $i";
	$resultado = $variable*$i;

	/*Cadena Heredoc, permite expandir variables en PHP*/
	$tablaDel9.=<<<HTML
			<tr>
				<td>$regla</td>
				<td>$resultado</td>
			</tr>
HTML;

}

$tablaDel9.=<<<HTML
	</table>
HTML;

echo $tablaDel9;

?>
</body>
</html>