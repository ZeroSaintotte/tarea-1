<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ejercicio 8</title>
</head>
<body>
<?php
	mt_srand(time());
	
	$contador = 0;
	
	for( $i = 0 ; $i < 900 ; $i++ )
	{
		$numeroAleatorio = mt_rand(1, 10000);
		if( ( $numeroAleatorio % 2 ) == 0 )
		{	
			echo $numeroAleatorio."<br/>";
			$contador++;
		}
	}
	
	echo "Se generaron $contador n&uacute;meros aleatorios pares.";
?>

</body>
</html>


