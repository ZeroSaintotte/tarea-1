<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        div {
        margin-bottom: 10px;
          display:inline-block;
          border: 1px solid black;
          border-radius: 20px;
          width: 40px;
          height: 40px;
        }
        div > p {
          margin-top: 11px;
          text-align: center;
          vertical-align: middle;
          margin-bottom: 20px;
          font-weight: bold;
        }
        #nv-0{
        	margin-left: 110px;
        }
        #nv-1{
        	margin-left: 70px;
        }
        #nv-2{
        	margin-left: 32px;
        }
        #nv-3{
        	margin-left: 7px;
        }
    </style>
    <title>ejercicio 16</title>
</head>
<body>

<?php
    $dat = array(3, 6, 4, 14, 9, 900, 45, 100, 30, 40, 15);
    $pat = array(-1, 3, 3, 6, 6, 4, 4, 14, 14, 9, 9);
    //Esto es para la impresion del arbol
    $imp = array(0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3);
    $arbol = crearArbol($dat, $pat, $imp);
    echo "<br><br>Recorrido de arbol en Pre-Orden: <br><br>";
    preorden($arbol);

    class Nodo {
        private $izquierda;
        private $derecha;
        private $valor;
        
        function __construct($var) {
            $this->izquierda = null;
            $this->derecha = null;
            $this->valor = $var;
        }

        function setIzquierda($nodo){
            $this->izquierda = $nodo;
        }
        function setDerecha($nodo){
            $this->derecha = $nodo;
        }
        function getIzquierda(){
            return $this->izquierda;
        }
        function getDerecha(){
            return $this->derecha;
        }
        function getValor(){
            return $this->valor;
        }
    }

    function crearArbol($datos, $patron, $nivel){
        $nv = 0;
        // creamos un array con todos los nodos
        for ($i = 0; $i < count($patron); $i++) {
            $map[$datos[$i]] = new Nodo($datos[$i]);
        }

        // creamos la raiz para el arbol
        $raiz = null;
    
        for ($i = 0; $i < count($patron); $i++) {
            // el primer dato siempre va a ser la raiz
            if ($patron[$i] == -1) {
                $raiz = $map[$datos[$i]];
                echo '<div id="nv-0"><p>' . $raiz->getValor() . "</p></div><br>";
                $nv++;
            }else{ //los demas datos van siguiendo el patron
                // tomamos el nodo que indique el patron
                $ptr = $map[$patron[$i]];
                //le buscamos en que lado el dato va a estar
                if ($ptr->getIzquierda() != null) {
                    $ptr->setDerecha($map[$datos[$i]]);
                }else{
                    $ptr->setIzquierda($map[$datos[$i]]);
                }
                // chequeamos cuando tenemos que bajar una linea para la impresion del arbol
                if($nivel[$i] > $nv){
                    echo "<br>";
                    $nv++;
                }
                echo '<div id="nv-'. $nv .'"><p>' . $map[$datos[$i]]->getValor() . "</p></div>";
            }
        }
        return $raiz;
    }

    function preorden($raiz){
        // segun entiendo: raiz-->todo izquierda--->todo derecha--> izquierda si continua ahi y/o luego derecha
        // hasta el final
        if ($raiz == null) {
            return;
        }
            
        echo $raiz->getValor()."&nbsp;&nbsp;";
        preorden($raiz->getIzquierda());
        preorden($raiz->getDerecha());
    }
?>

</body>
</html>