<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        div {
        margin-bottom: 10px;
          display:inline-block;
          border: 1px solid black;
          border-radius: 20px;
          width: 40px;
          height: 40px;
        }
        div > p {
          margin-top: 11px;
          text-align: center;
          vertical-align: middle;
          margin-bottom: 20px;
          font-weight: bold;
        }
        #nv-0{
        	margin-left: 185px;
        }
        #nv-1{
        	margin-left: 75px;
        }
        #nv-2{
        	margin-left: 27px;
        }
        #nv-3{
        	margin-left: 7px;
        }
    </style>
    <title>ejercicio 17</title>
</head>
<body>

<?php
	echo "Arbol n-nario<br><br>";
    $dat = array("R", "S", "T", "D", "U", "V", "G", "F", "H", "J", "L", "Q", "W", "C", "K", "A", "X", "Z", "I");
    $pat = array("e", "R", "R", "R", "S", "S", "T", "T", "D", "D", "U", "U", "U", "G", "G", "H", "H", "J", "J");
    $imp = array(0, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3);
    $arbol = crearArbol($dat, $pat, $imp);

    class Nodo {

        private $hijos;
        private $valor;

         function __construct($var) {
            $this->hijos = array();
            $this->valor = $var;
        }

        function agregarNodo($nodo){
           array_push($this->hijos, $nodo);
        }
        function getValor(){
            return $this->valor;
        }
    }

    function crearArbol($datos, $patron, $nivel){
        // la misma idea que el otro solo que es un array de nodos envez de nodo izquierda y nodo derecha
        $nv = 0;
        for ($i = 0; $i < count($patron); $i++) {
            $map[$datos[$i]] = new Nodo($datos[$i]);
        }

        $raiz = null;
    
        for ($i = 0; $i < count($patron); $i++) {
            if ($patron[$i] == "e") {
                $raiz = $map[$datos[$i]];
                echo '<div id="nv-0"><p>' . $raiz->getValor() . "</p></div><br>";
                $nv++;
            }else{
                $ptr = $map[$patron[$i]];

                $ptr->agregarNodo($map[$datos[$i]]);

                if($nivel[$i] > $nv){
                    echo "<br>";
                    $nv++;
                }
                echo '<div id="nv-'. $nv .'"><p>' . $map[$datos[$i]]->getValor() . "</p></div>";
            }
        }
        return $raiz;
    }
?>

</body>
</html>