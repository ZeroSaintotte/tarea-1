<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Ejercicio 8</title>
<link href="css/estilo_tabla.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<?php
mt_srand(time());
    function build_table($array){

    $html = '<table>';

    $html .= '<tr>';
    foreach($array[0] as $key=>$value){
            $html .= '<th>' . htmlspecialchars($key) . '</th>';
        }
    $html .= '</tr>';


    foreach( $array as $key=>$value){
        $html .= '<tr>';
        foreach($value as $key2=>$value2){
            $html .= '<td>' . htmlspecialchars($value2) . '</td>';
        }
        $html .= '</tr>';
    }



    $html .= '</table>';
    return $html;
}

$array = array (
  array(mt_rand(1,100),mt_rand(1,100),mt_rand(1,100),mt_rand(1,100)),
  array(mt_rand(1,100),mt_rand(1,100),mt_rand(1,100),mt_rand(1,100)),
  array(mt_rand(1,100),mt_rand(1,100),mt_rand(1,100),mt_rand(1,100)),
  array(mt_rand(1,100),mt_rand(1,100),mt_rand(1,100),mt_rand(1,100))
);

echo build_table($array);
?>


</body>
</html>