/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases V7.3.4                     */
/* Target DBMS:           PostgreSQL 9                                    */
/* Project file:          FinalSegunda.dez                                */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Database creation script                        */
/* Created on:            2023-01-24 23:15                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Tables                                                                 */
/* ---------------------------------------------------------------------- */

/* ---------------------------------------------------------------------- */
/* Add table "cursos"                                                     */
/* ---------------------------------------------------------------------- */

CREATE TABLE cursos (
    id SERIAL  NOT NULL,
    nombre TEXT  NOT NULL,
    ano INTEGER  NOT NULL,
    seccion CHARACTER(1)  NOT NULL,
    CONSTRAINT PK_cursos PRIMARY KEY (id)
);

/* ---------------------------------------------------------------------- */
/* Add table "alumnos"                                                    */
/* ---------------------------------------------------------------------- */

CREATE TABLE alumnos (
    id SERIAL  NOT NULL,
    nombre TEXT  NOT NULL,
    apellido TEXT  NOT NULL,
    matricula TEXT  NOT NULL,
    CONSTRAINT PK_alumnos PRIMARY KEY (id)
);

/* ---------------------------------------------------------------------- */
/* Add table "inscripciones"                                              */
/* ---------------------------------------------------------------------- */

CREATE TABLE inscripciones (
    id SERIAL  NOT NULL,
    curso_id INTEGER  NOT NULL,
    alumno_id INTEGER  NOT NULL,
    fecha DATE  NOT NULL,
    activo BOOLEAN  NOT NULL,
    CONSTRAINT PK_inscripciones PRIMARY KEY (id)
);

/* ---------------------------------------------------------------------- */
/* Foreign key constraints                                                */
/* ---------------------------------------------------------------------- */

ALTER TABLE inscripciones ADD CONSTRAINT cursos_inscripciones 
    FOREIGN KEY (curso_id) REFERENCES cursos (id);

ALTER TABLE inscripciones ADD CONSTRAINT alumnos_inscripciones 
    FOREIGN KEY (alumno_id) REFERENCES alumnos (id);
